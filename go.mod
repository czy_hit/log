module gitee.com/czy_hit/log

go 1.21.0

require (
	github.com/deckarep/golang-set v1.8.0
	github.com/gorilla/websocket v1.5.1
	go.uber.org/zap v1.25.0
)

require (
	go.uber.org/multierr v1.10.0 // indirect
	golang.org/x/net v0.17.0 // indirect
)
