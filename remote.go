package log

import (
	"net/http"
	"os"
	"sync"

	mapset "github.com/deckarep/golang-set"
	"github.com/gorilla/websocket"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func remoteLogRegister() {
	http.HandleFunc("/log", connectHandler)
}

func remoteLogOption() zap.Option {
	return zap.Hooks(remoteWrite)
}

var connSet = mapset.NewSet()

func connectHandler(w http.ResponseWriter, r *http.Request) {
	upgrader := websocket.Upgrader{
		// solve cross domain problem
		CheckOrigin: func(*http.Request) bool {
			return true
		},
	}
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		os.Stderr.WriteString("[remote log] failed to upgrade from http to websocket because of " + err.Error() + "\n")
		return
	}

	ok := connSet.Add(conn)
	if !ok {
		os.Stderr.WriteString("[remote log]failed to add new conn to conn set because of " + err.Error() + "\n")
		return
	}
}

var rLogMtx = sync.Mutex{}

func remoteWrite(entry zapcore.Entry) error {
	rLogMtx.Lock()
	defer rLogMtx.Unlock()

	closed_conn := mapset.NewThreadUnsafeSet()

	for untyped_conn := range connSet.Iter() {
		conn, ok := untyped_conn.(*websocket.Conn)
		if !ok || conn == nil {
			os.Stderr.WriteString("[remote log] conn set contains type other than *websocket.Conn" + "\n")
			continue

		}
		err := conn.WriteJSON(entry)
		if err != nil {
			closed_conn.Add(conn)
		}
	}

	// 清理已经关闭的连接
	for untyped_conn := range closed_conn.Iter() {
		conn, ok := untyped_conn.(*websocket.Conn)
		if !ok || conn == nil {
			os.Stderr.WriteString("[remote log] conn set contains type other than *websocket.Conn" + "\n")
			continue

		}
		connSet.Remove(conn)
	}
	return nil
}
