package log

import (
	"os"

	"go.uber.org/zap"
)

type RawLogger struct {
	*zap.Logger
}

var (
	rawLogger *RawLogger
	logger    Logger
)

var (
	Debugf func(string, ...interface{})
	Infof  func(string, ...interface{})
	Warnf  func(string, ...interface{})
	Errorf func(string, ...interface{})
	Fatalf func(string, ...interface{})

	Debug func(args ...interface{})
	Info  func(args ...interface{})
	Warn  func(args ...interface{})
	Error func(args ...interface{})
	Fatal func(args ...interface{})
	Sync  func() error
)

type Logger interface {
	Debugf(string, ...interface{})
	Infof(string, ...interface{})
	Warnf(string, ...interface{})
	Errorf(string, ...interface{})
	Fatalf(string, ...interface{})

	Debug(args ...interface{})
	Info(args ...interface{})
	Warn(args ...interface{})
	Error(args ...interface{})
	Fatal(args ...interface{})

	Sync() error
}

func setDefaultLoggerFunc() {
	Debugf = logger.Debugf
	Infof = logger.Infof
	Warnf = logger.Warnf
	Errorf = logger.Errorf
	Fatalf = logger.Fatalf
	Debug = logger.Debug
	Info = logger.Info
	Warn = logger.Warn
	Error = logger.Error
	Fatal = logger.Fatal
	Sync = logger.Sync
}

type NoopLogger struct{}

func (l *NoopLogger) Fatalf(format string, v ...interface{}) {
}

func (l *NoopLogger) Errorf(format string, v ...interface{}) {
}

func (l *NoopLogger) Warnf(format string, v ...interface{}) {
}

func (l *NoopLogger) Infof(format string, v ...interface{}) {
}

func (l *NoopLogger) Debugf(format string, v ...interface{}) {
}

func (l *NoopLogger) Fatal(v ...interface{}) {
}

func (l *NoopLogger) Error(v ...interface{}) {
}

func (l *NoopLogger) Warn(v ...interface{}) {
}

func (l *NoopLogger) Info(v ...interface{}) {
}

func (l *NoopLogger) Debug(v ...interface{}) {
}

func (l *NoopLogger) Sync() error {
	return nil
}

func init() {
	logger = &NoopLogger{}
	setDefaultLoggerFunc()
}

func NewLogger() (Logger, error) {
	var err error
	var l *zap.Logger

	softbusDebugFlag := os.Getenv("SOFTBUS_DEBUG")
	softbusRLogFlag := os.Getenv("SOFTBUS_RLOG")

	options := make([]zap.Option, 0)
	if softbusRLogFlag == "true" {
		remoteLogRegister()
		options = append(options, remoteLogOption())
	}

	if softbusDebugFlag == "true" {
		l, err = zap.NewDevelopment(options...)
	} else {
		l, err = zap.NewProduction(options...)
	}
	if err != nil {
		return nil, err
	}

	rawLogger = &RawLogger{l}
	logger = l.Sugar()
	setDefaultLoggerFunc()

	return logger, nil
}

func GetRawLogger() *RawLogger {
	return rawLogger
}

func GetLogger() Logger {
	return logger
}
