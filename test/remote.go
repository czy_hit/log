package main

import (
	"net/http"
	"os"
	"time"

	"gitee.com/czy_hit/log"
)

func main() {
	os.Setenv("SOFTBUS_DEBUG", "false")
	os.Setenv("SOFTBUS_RLOG", "true")
	_, err := log.NewLogger()
	if err != nil {
		os.Stderr.WriteString("[test remote log] failed to new logger because of " + err.Error() + "\n")
		return
	}

	go func() {
		err := http.ListenAndServe("0.0.0.0:1543", nil)
		if err != nil {
			os.Stderr.WriteString("[test remote log] failed to enable http server because of " + err.Error() + "\n")
			return
		}
	}()

	ticker := time.NewTicker(time.Second * time.Duration(4))
	for {
		<-ticker.C
		log.GetLogger().Infof("test remote log")
	}
}
