package log

import (
	"os"
	"testing"
)

func TestDevelopmentLog(t *testing.T) {
	// Just need to run it in main codes.
	os.Setenv("SOFTBUS_DEBUG", "true")
	_, err := NewLogger()
	if err != nil {
		t.Fatal(err)
	}

	// User code start here.
	debugLog := GetLogger()

	defer func() {
		if err := recover(); err != nil {
			t.Logf("recover:%v", err)
		} else {
			t.Fatal("Panic not caught")
		}
	}()
	debugLog.Debugf("debug format")
	debugLog.Warnf("warn format")
	debugLog.Infof("info format")
	debugLog.Errorf("error format")
	debugLog.Fatalf("fatal format")
}

func TestProductionLog(t *testing.T) {
	// Just need to run it in main codes.
	os.Setenv("SOFTBUS_DEBUG", "false")
	_, err := NewLogger()
	if err != nil {
		t.Fatal(err)
	}

	// User code start here.
	log := GetLogger()
	defer func() {
		if err := recover(); err != nil {
			t.Logf("recover:%v", err)
		} else {
			t.Fatal("Panic not caught")
		}
	}()
	log.Debugf("debug format")
	log.Warnf("warn format")
	log.Infof("info format")
	log.Errorf("error format")
	log.Fatalf("fatal format")
}
